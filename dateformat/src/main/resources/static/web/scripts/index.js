document.addEventListener("DOMContentLoaded", () => {
    var app = new Vue({
        el: '#app',
        // vuetify: new Vuetify(),
        data: {
            formats: [],
            formatRefreshInterval: null,
            enableFormatRefresh: false,
            formatStringFocus : false,
            dateStringFocus : false,
            evaluationData: {
                dateString: '',
                formatString: ''
            },
            evaluationResultData: {
            },
            evaluationRun: false,
            evaluationRunning: false
        },
        methods: {
            refreshFormatList: function () {
                if (this.enableFormatRefresh || this.formats.length === 0) {
                    let self = this;
                    axios.get('/api/v1/date-formats', {
                        params: {
                        }
                    }).then(function (response) {
                        self.formats = response.data.formatDetails;
                    }).catch(function (error) {
                        console.log(error);
                    }).then(function () {
                        // always executed
                    });
                }
            },
            evaluateDateFormat: function () {
                this.evaluationRunning = true;
                let self = this;
                axios.post('/api/v1/evaluate-date-format', this.evaluationData)
                        .then(function (response) {
                            self.evaluationResultData = response.data;
                            self.evaluationRun = true;
                        }).catch(function (error) {
                    console.log(error);
                }).then(function () {
                    self.evaluationRunning = false;
                    self.evaluationRun = true;
                    // always executed
                });
            },
            startInterval: function () {
                setInterval(() => {
                    this.refreshFormatList();
                }, 1000);
            }
        }
        , mounted: function () {
            this.refreshFormatList();
            //this.formatRefreshInterval = setInterval(this.refreshFormatList(), 1000);
            this.startInterval();


        }, computed: {
            showCompileSuccess: function () {
                return !this.evaluationRunning && this.evaluationRun && this.evaluationResultData.formatCompileSuccessful;
            },
            showCompileFailure: function () {
                return !this.evaluationRunning && this.evaluationRun && !this.evaluationResultData.formatCompileSuccessful;
            },
            showParseSuccess: function () {
                return !this.evaluationRunning && this.evaluationRun && this.evaluationResultData.formatCompileSuccessful && this.evaluationResultData.dateParseSuccessful;
            },
            showParseFailure: function () {
                return !this.evaluationRunning &&
                        this.evaluationRun &&
                        this.evaluationResultData.formatAndDateStringSpecified &&
                        this.evaluationResultData.formatCompileSuccessful &&
                        !this.evaluationResultData.dateParseSuccessful;
            }
        }
    })
});
