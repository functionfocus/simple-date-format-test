package com.functionfocus.dateformat.rest;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DateFormatController {

    /**
     * The number of lines of Stacktrace to report in the response. The number
     * is limited so that the resulting stacktrace is limited to the most
     * relevant lines, not the application code.
     */
    public static final int STACKTRACE_REPORTING_DEPTH = 2;

    @PostMapping("/api/v1/evaluate-date-format")
    public EvaluateDateFormatResponseModel evaluateDateFormat(@RequestBody EvaluateDateFormatRequestModel requestModel) {
        EvaluateDateFormatResponseModel responseModel = new EvaluateDateFormatResponseModel();
        if (requestModel.getDateString() != null && requestModel.getFormatString() != null) {
            DateFormat dateFormat;
            responseModel.setFormatNotSpecified(requestModel.getFormatString().isEmpty());
            responseModel.setFormatOnlySpecified(!requestModel.getFormatString().isEmpty() && requestModel.getDateString().isEmpty());
            responseModel.setFormatAndDateStringSpecified(!requestModel.getFormatString().isEmpty() && !requestModel.getDateString().isEmpty());

            responseModel.setInputFormatString(requestModel.getFormatString());
            responseModel.setInputDateString(requestModel.getDateString());

            try {
                dateFormat = new SimpleDateFormat(requestModel.getFormatString());
                responseModel.setFormatCompileSuccessful(true);
            } catch (Exception e) {
                dateFormat = null;
                responseModel.setFormatCompileSuccessful(false);
                responseModel.setJavaStaceTrace(parseToString(e, STACKTRACE_REPORTING_DEPTH));
                responseModel.setJavaErrorMessage(e.getMessage());
            }
            if (dateFormat != null && responseModel.isFormatAndDateStringSpecified()) {
                try {
                    Date parsedDate = dateFormat.parse(requestModel.getDateString());
                    responseModel.setFormattedDateString(dateFormat.format(parsedDate));
                    responseModel.setJavaDateString(parsedDate);
                    responseModel.setDateParseSuccessful(true);
                } catch (ParseException e) {
                    responseModel.setDateParseSuccessful(false);
                    responseModel.setJavaStaceTrace(parseToString(e, STACKTRACE_REPORTING_DEPTH));
                    responseModel.setJavaErrorMessage(e.getMessage());
                }
            }
        }

        //responseModel.setMessage(String.format("You sent %s and %s at %s", requestModel.getFormatString(), requestModel.getDateString(), new Date().toString()));
        return responseModel;
    }

    /**
     * Convert the Throwable into a String with a maximum number of lines
     * specified
     *
     * @param e
     * @param linesToKeep
     * @return
     */
    private String parseToString(final Throwable e, final int linesToKeep) {
        // transform the Throwable into a String
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));

        // split the stacktrace into the defined number of lines and re-join those into a single string
        // we're adding one to this index so that we can discard the remaining stack trace elements which
        // will populate this last array element
        String[] parts = StringUtils.split(sw.toString(), "\n", linesToKeep + 1);

        // discard the last element in the array; this last element contains the 
        // remainder of the stack trace that we want to supress
        parts[parts.length - 1] = null;

        return StringUtils.join(parts, '\n');
    }

    @GetMapping("/api/v1/date-formats")
    public EnumerateDateFormatResponseModel test() {

        Date currentDate = new Date();
        Date fixedDate = null;
        try {
            fixedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse("2001-07-04T12:08:56.235-07:00");
        } catch (Exception e) {
            e.printStackTrace();
        }
        long start = System.nanoTime();
        EnumerateDateFormatResponseModel responseModel = EnumerateDateFormatResponseModel.buildModel(fixedDate, currentDate);
        long end = System.nanoTime();
        //System.out.println("operation took " + ((end - start)/1000000) + "ms");
        return responseModel;
    }

}
