package com.functionfocus.dateformat.rest;

/**
 *
 * @author matthew
 */
public class EvaluateDateFormatRequestModel {

    private String formatString;
    private String dateString;

    public EvaluateDateFormatRequestModel() {

    }

    /**
     * @return the formatString
     */
    public String getFormatString() {
        return formatString;
    }

    /**
     * @param formatString the formatString to set
     */
    public void setFormatString(String formatString) {
        this.formatString = formatString;
    }

    /**
     * @return the dateString
     */
    public String getDateString() {
        return dateString;
    }

    /**
     * @param dateString the dateString to set
     */
    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

}
