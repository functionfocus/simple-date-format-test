package com.functionfocus.dateformat.rest;

import java.util.Date;

/**
 *
 * @author matthew
 */
public final class DateFormatValidationResponseModel {

    private String dateString;
    private String formatString;
    private boolean dateStringValid;
    private boolean formatStringValid;
    private Date parsedDate;

    public DateFormatValidationResponseModel() {

    }

    /**
     * @return the dateString
     */
    public String getDateString() {
        return dateString;
    }

    /**
     * @param dateString the dateString to set
     */
    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    /**
     * @return the formatString
     */
    public String getFormatString() {
        return formatString;
    }

    /**
     * @param formatString the formatString to set
     */
    public void setFormatString(String formatString) {
        this.formatString = formatString;
    }

    /**
     * @return the dateStringValid
     */
    public boolean isDateStringValid() {
        return dateStringValid;
    }

    /**
     * @param dateStringValid the dateStringValid to set
     */
    public void setDateStringValid(boolean dateStringValid) {
        this.dateStringValid = dateStringValid;
    }

    /**
     * @return the formatStringValid
     */
    public boolean isFormatStringValid() {
        return formatStringValid;
    }

    /**
     * @param formatStringValid the formatStringValid to set
     */
    public void setFormatStringValid(boolean formatStringValid) {
        this.formatStringValid = formatStringValid;
    }

    /**
     * @return the parsedDate
     */
    public Date getParsedDate() {
        return parsedDate;
    }

    /**
     * @param parsedDate the parsedDate to set
     */
    public void setParsedDate(Date parsedDate) {
        this.parsedDate = parsedDate;
    }
}
