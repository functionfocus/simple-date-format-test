package com.functionfocus.dateformat.rest;

import com.functionfocus.dateformat.formats.DateFormatModel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.thymeleaf.util.StringUtils;

/**
 *
 * @author matthew
 */
public final class EnumerateDateFormatResponseModel {

    private List<IndividualDateFormatDetailModel> formatDetails;

    public static EnumerateDateFormatResponseModel buildModel(Date fixedDate, Date currentDate) {
        EnumerateDateFormatResponseModel instance = new EnumerateDateFormatResponseModel();
        for (DateFormatModel dateFormatModel : DateFormatModel.values()) {
            IndividualDateFormatDetailModel model = new IndividualDateFormatDetailModel();
            //model.setCharacter(dateFormatModel.getLetter());
            model.setDateTimeComponent(dateFormatModel.getDateTimeComponent());
            model.setNotes(dateFormatModel.getNotes());
            for (int i = 1; i <= dateFormatModel.getRepetitionLimit(); i++) {
                String format = StringUtils.repeat(dateFormatModel.getLetter(), i);
                try {
                    DateFormat df = new SimpleDateFormat(format);
                    model.getCharacters().add(format);
                    if (fixedDate != null) {
                        model.getFixedExamples().add(df.format(fixedDate));
                    }

                    if (currentDate != null) {
                        model.getNowExamples().add(df.format(currentDate));
                    }

                } catch (Exception e) {
                    //LOG.error("Error parsing date format {}", format);
                    System.err.println(String.format("Error parsing date format %s", format));
                }
            }
            instance.getFormatDetails().add(model);
        }
        return instance;
    }

    public EnumerateDateFormatResponseModel() {

    }

    /**
     * @return the formatDetails
     */
    public List<IndividualDateFormatDetailModel> getFormatDetails() {
        if (formatDetails == null) {
            this.setFormatDetails(new ArrayList<>());
        }
        return formatDetails;
    }

    /**
     * @param formatDetails the formatDetails to set
     */
    public void setFormatDetails(List<IndividualDateFormatDetailModel> formatDetails) {
        this.formatDetails = formatDetails;
    }

}
