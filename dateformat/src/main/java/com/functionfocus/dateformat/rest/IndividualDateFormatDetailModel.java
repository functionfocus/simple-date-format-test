package com.functionfocus.dateformat.rest;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author matthew
 */
public final class IndividualDateFormatDetailModel {

    private List<String> characters;
    private String dateTimeComponent;
    private String presentation;
    private String notes;
    private List<String> fixedExamples;
    private List<String> nowExamples;

    public IndividualDateFormatDetailModel() {

    }

//    /**
//     * @return the character
//     */
//    public String getCharacter() {
//        return character;
//    }
//
//    /**
//     * @param character the character to set
//     */
//    public void setCharacter(String character) {
//        this.character = character;
//    }

    /**
     * @return the dateTimeComponent
     */
    public String getDateTimeComponent() {
        return dateTimeComponent;
    }

    /**
     * @param dateTimeComponent the dateTimeComponent to set
     */
    public void setDateTimeComponent(String dateTimeComponent) {
        this.dateTimeComponent = dateTimeComponent;
    }

    /**
     * @return the presentation
     */
    public String getPresentation() {
        return presentation;
    }

    /**
     * @param presentation the presentation to set
     */
    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the fixedExamples
     */
    public List<String> getFixedExamples() {
        if (this.fixedExamples == null) {
            this.setFixedExamples(new ArrayList<>());
        }
        return fixedExamples;
    }

    /**
     * @param fixedExamples the fixedExamples to set
     */
    public void setFixedExamples(List<String> fixedExamples) {
        this.fixedExamples = fixedExamples;
    }

    /**
     * @return the nowExamples
     */
    public List<String> getNowExamples() {
        if (this.nowExamples == null) {
            this.setNowExamples(new ArrayList<>());
        }
        return nowExamples;
    }

    /**
     * @param nowExamples the nowExamples to set
     */
    public void setNowExamples(List<String> nowExamples) {
        this.nowExamples = nowExamples;
    }

    /**
     * @return the characters
     */
    public List<String> getCharacters() {
        if(characters == null){
        this.setCharacters(new ArrayList<>());
        }
        return characters;
    }

    /**
     * @param characters the characters to set
     */
    public void setCharacters(List<String> characters) {
        this.characters = characters;
    }

}
