package com.functionfocus.dateformat.rest;

/**
 *
 * @author matthew
 */
public final class DateFormatValidationRequestModel {

    private String dateString;
    private String formatString;
    
    public DateFormatValidationRequestModel() {

    }

    /**
     * @return the dateString
     */
    public String getDateString() {
        return dateString;
    }

    /**
     * @param dateString the dateString to set
     */
    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    /**
     * @return the formatString
     */
    public String getFormatString() {
        return formatString;
    }

    /**
     * @param formatString the formatString to set
     */
    public void setFormatString(String formatString) {
        this.formatString = formatString;
    }

}
