package com.functionfocus.dateformat.rest;

import java.util.Date;

/**
 *
 * @author matthew
 */
public class EvaluateDateFormatResponseModel {

    private String inputDateString;
    private String inputFormatString;
    private boolean dateParseSuccessful;
    private boolean formatCompileSuccessful;
    private String javaErrorMessage;
    private Date javaDateString;
    private String formattedDateString;
    private String javaStaceTrace;
    private boolean formatNotSpecified;
    private boolean formatOnlySpecified;
    private boolean formatAndDateStringSpecified;

    public EvaluateDateFormatResponseModel() {

    }

    /**
     * @return the inputDateString
     */
    public String getInputDateString() {
        return inputDateString;
    }

    /**
     * @param inputDateString the inputDateString to set
     */
    public void setInputDateString(String inputDateString) {
        this.inputDateString = inputDateString;
    }

    /**
     * @return the inputFormatString
     */
    public String getInputFormatString() {
        return inputFormatString;
    }

    /**
     * @param inputFormatString the inputFormatString to set
     */
    public void setInputFormatString(String inputFormatString) {
        this.inputFormatString = inputFormatString;
    }

    /**
     * @return the dateParseSuccessful
     */
    public boolean isDateParseSuccessful() {
        return dateParseSuccessful;
    }

    /**
     * @param dateParseSuccessful the dateParseSuccessful to set
     */
    public void setDateParseSuccessful(boolean dateParseSuccessful) {
        this.dateParseSuccessful = dateParseSuccessful;
    }

    /**
     * @return the formatCompileSuccessful
     */
    public boolean isFormatCompileSuccessful() {
        return formatCompileSuccessful;
    }

    /**
     * @param formatCompileSuccessful the formatCompileSuccessful to set
     */
    public void setFormatCompileSuccessful(boolean formatCompileSuccessful) {
        this.formatCompileSuccessful = formatCompileSuccessful;
    }

    /**
     * @return the javaErrorMessage
     */
    public String getJavaErrorMessage() {
        return javaErrorMessage;
    }

    /**
     * @param javaErrorMessage the javaErrorMessage to set
     */
    public void setJavaErrorMessage(String javaErrorMessage) {
        this.javaErrorMessage = javaErrorMessage;
    }

    /**
     * @return the javaDateString
     */
    public Date getJavaDateString() {
        return javaDateString;
    }

    /**
     * @param javaDateString the javaDateString to set
     */
    public void setJavaDateString(Date javaDateString) {
        this.javaDateString = javaDateString;
    }

    /**
     * @return the formattedDateString
     */
    public String getFormattedDateString() {
        return formattedDateString;
    }

    /**
     * @param formattedDateString the formattedDateString to set
     */
    public void setFormattedDateString(String formattedDateString) {
        this.formattedDateString = formattedDateString;
    }

    /**
     * @return the javaStaceTrace
     */
    public String getJavaStaceTrace() {
        return javaStaceTrace;
    }

    /**
     * @param javaStaceTrace the javaStaceTrace to set
     */
    public void setJavaStaceTrace(String javaStaceTrace) {
        this.javaStaceTrace = javaStaceTrace;
    }

    /**
     * @return the formatNotSpecified
     */
    public boolean isFormatNotSpecified() {
        return formatNotSpecified;
    }

    /**
     * @param formatNotSpecified the formatNotSpecified to set
     */
    public void setFormatNotSpecified(boolean formatNotSpecified) {
        this.formatNotSpecified = formatNotSpecified;
    }

    /**
     * @return the formatOnlySpecified
     */
    public boolean isFormatOnlySpecified() {
        return formatOnlySpecified;
    }

    /**
     * @param formatOnlySpecified the formatOnlySpecified to set
     */
    public void setFormatOnlySpecified(boolean formatOnlySpecified) {
        this.formatOnlySpecified = formatOnlySpecified;
    }

    /**
     * @return the formatAndDateStringSpecified
     */
    public boolean isFormatAndDateStringSpecified() {
        return formatAndDateStringSpecified;
    }

    /**
     * @param formatAndDateStringSpecified the formatAndDateStringSpecified to
     * set
     */
    public void setFormatAndDateStringSpecified(boolean formatAndDateStringSpecified) {
        this.formatAndDateStringSpecified = formatAndDateStringSpecified;
    }

}
