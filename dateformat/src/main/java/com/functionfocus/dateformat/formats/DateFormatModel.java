package com.functionfocus.dateformat.formats;

/**
 * This enum lists the available date format Strings and metadata associated
 * with each.
 *
 * @see derived from
 * https://cr.openjdk.java.net/~iris/se/11/latestSpec/api/java.base/java/text/SimpleDateFormat.html
 * @author matthew
 */
public enum DateFormatModel {

    G("G", "Era", "Era", 1,""),
    y("y", "Year", "Year", 2,""),
    Y("Y", "Week", "Week year", 2,""),
    M("M", "Month", "Month in year (context sensitive)", 4,"Single character will suppress leading zeros"),
    x("L", "Week", "Month in year (standalone form)", 4,"Single character will suppress leading zeros"),
    w("w", "Week", "Week in year", 2,""),
    W("W", "Week", "Week in month", 1,""),
    D("D", "Date", "Day in year", 2,"Single character will suppress leading zeros"),
    d("d", "Date", "Day in month", 2,""),
    F("F", "Date", "Day of week in month", 1,""),
    E("E", "Date", "Day name in week", 4,""),
    u("u", "Date", "Day number of week(1 = Monday, 7 = Sunday)", 1,""),
    a("a", "Time", "Am/pm marker", 1,"AM or PM"),
    H("H", "Time", "Hour in day (0-23)", 1,"Single character will suppress leading zeros"),
    k("k", "Time", "Hour in day (1-24)", 1,"Single character will suppress leading zeros"),
    K("K", "Time", "Hour in am/pm (0-11)", 2,"Single character will suppress leading zero"),
    h("h", "Time", "Hour in am/pm (1-12)", 2,"Single character will suppress leading zero"),
    m("m", "Time", "Minute in hour", 2,"Single character will suppress leading zeros"),
    s("s", "Time", "Second in minute", 2,"Single character will suppress leading zeros"),
    S("S", "Time", "Millisecond", 3,"Single character will suppress leading zeros"),
    z("z", "Time", "General time zone", 4,""),
    Z("Z", "Time", "RFC 822 time zone", 4,""),
    X("X", "Time", "ISO 8601 time zone", 4,"");
    private String letter;
    private String dateTimeComponent;
    private int repetitionLimit;
    private String notes;
    private String category;

    private DateFormatModel(String letter, String category, String dateTimeComponent, int repetitionLimit, String notes) {
        this.setLetter(letter);
        this.setCategory(category);
        this.setDateTimeComponent(dateTimeComponent);
        this.setRepetitionLimit(repetitionLimit);
        this.setNotes(notes);
    }

    /**
     * @return the letter
     */
    public String getLetter() {
        return letter;
    }

    /**
     * @param letter the letter to set
     */
    private void setLetter(String letter) {
        this.letter = letter;
    }

    /**
     * @return the letter
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param letter the letter to set
     */
    private void setCategory(String category) {
        this.category = category;
    }

    
    
    /**
     * @return the dateTimeComponent
     */
    public String getDateTimeComponent() {
        return dateTimeComponent;
    }

    /**
     * @param dateTimeComponent the dateTimeComponent to set
     */
    private void setDateTimeComponent(String dateTimeComponent) {
        this.dateTimeComponent = dateTimeComponent;
    }

    /**
     * @return the repetitionLimit
     */
    public int getRepetitionLimit() {
        return repetitionLimit;
    }

    /**
     * @param repetitionLimit the repetitionLimit to set
     */
    private void setRepetitionLimit(int repetitionLimit) {
        this.repetitionLimit = repetitionLimit;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }
}
