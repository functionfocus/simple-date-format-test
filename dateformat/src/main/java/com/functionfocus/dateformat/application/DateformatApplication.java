package com.functionfocus.dateformat.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@ComponentScan("com.functionfocus.dateformat.mvc")
@ComponentScan("com.functionfocus.dateformat.rest")
public class DateformatApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(DateformatApplication.class, args);
    }

}
